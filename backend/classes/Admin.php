<?php

class Admin
{
    private $_db;
    private $id;
    private $username;
    private $password;
    private $email;
    private $isActive;

    public function __construct($user = null)
    {
        $this->_db = Database::getInstance();
    }

    public function login($username, $password, $remember = false)
    {

        $res = $this->getAllAdmin();
        foreach ($res as $user){
            if ($user->username == $username){
                if ($user->password == $password){
                    $this->username = $username;
                    $this->password = $password;
                    return $user;
                }
            }
        }
        return false;
    }

    public function logout()
    {
        Session::delete("admin");
    }

    public function getAllAdmin(){
        $this->_db->get("admins", []);
        return $this->_db->results();
    }

    public function create($fields = array())
    {
        if (!$this->_db->insert('admins', $fields))
        {
            throw new Exception("Unable to create the admin.");
        }
    }

    public function delete($id)
    {

        if (!$this->_db->delete('admins', array('id', '=', $id)))
        {
            throw new Exception('Unable to update the user.');
        }
    }

}