<?php


class Clinic
{
    private $_db;
    private $id;
    private $name;
    private $address;
    private $isActive;
    private $phone;
    private $isFullTime;
    private $deletedAt;
    private $createdAt;
    private $updatedAt;

    public function __construct($user = null)
    {
        $this->_db = Database::getInstance();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }


    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setAddress($address): void
    {
        $this->address = $address;
    }

    public function getIsActive()
    {
        return $this->isActive;
    }

    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    public function getIsFullTime()
    {
        return $this->isFullTime;
    }

    public function setIsFullTime($isFullTime): void
    {
        $this->isFullTime = $isFullTime;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }


    public function getDb(): ?Database
    {
        return $this->_db;
    }

    public function setDb(?Database $db): void
    {
        $this->_db = $db;
    }

    public function getAllClinic()
    {
        $this->_db->get("clinics", []);
        return $this->_db->results();
    }

    public function create($fields = array())
    {
        if (!$this->_db->insert('clinics', $fields)) {
            throw new Exception("Unable to create the Clinic.");
        }
    }

    public function update($fields = array(), $id = null)
    {

        if (!$this->_db->update('clinics', $id, $fields))
        {
            throw new Exception('Unable to update the user.');
        }
    }

    public function delete($id)
    {
        if (!$this->_db->delete('clinics', array('id', '=', $id))) {
            throw new Exception('Unable to delete the Clinic.');
        }
    }

}
