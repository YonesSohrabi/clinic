<?php
require_once "../start.php";

if (isset($_POST["login-btn"])) {
    $email = $_POST["email"];
    $pass = hash("md5",$_POST["password"]);
    $isLogged = false;
    if (isset($_POST["isAdmin"])) {
        $adm = new Admin();
        if ($admin = $adm->login($email, $pass)) {
            Session::put("login", "admin");
            Session::put("loggedID", "$admin->id");
            $isLogged = true;
            Redirect::to("index.php");
        }
    }
    if (!$isLogged){
        Redirect::to("signin.php");
    }
}

if (isset($_POST["addAdminBTN"])) {
    $email = $_POST["email_admin"];
    $pass = hash("md5",$_POST["pass_admin"]);
    $username = $_POST["username_admin"];
    $value = ["id"=>"", "username"=>$username,"password"=>$pass,"email"=>$email,"is_active"=>1];
    $adm = new Admin();
    $adm->create($value);
    Redirect::to("admins.php");
}

if (isset($_POST["addClinicBTN"])) {
    $name = $_POST["clinic_name"];
    $phone = $_POST["clinic_phone"];
    $address = $_POST["clinic_address"];
    $isFullTime = $_POST["clinic_fTime"];
    $value = ["id"=>"", "name"=>$name,"address"=>$address, "is_active"=>1,"phone"=>$phone,
        "is_full_time"=>$isFullTime];
    $clinic = new Clinic();
    $clinic->create($value);
    Redirect::to("clinics.php");
}

if (isset($_GET["adminDelID"])){
    $adminID = $_GET["adminDelID"];
    (new Admin())->delete($adminID);
    Redirect::to("admins.php");
}

if (isset($_POST["clinic_del_id"])){
    $clnc = new Clinic();
    $clinicList = $clnc->getAllClinic() ;
    $clinicID = $_POST['clinic_del_id'];
    $sql = "UPDATE clinics SET `deleted_at` = ?,`is_active` = ? WHERE id = ?;";
    $nowDate = date("Y-m-d");
    $clnc->getDb()->query($sql,[$nowDate,0,$clinicID]);

    echo json_encode("salam");
}