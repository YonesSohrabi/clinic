<?php ?>

<!-- Chart library -->
<script src="frontend/assets/plugins/chart.min.js"></script>
<!-- Icons library -->
<script src="frontend/assets/plugins/feather.min.js"></script>
<!-- jQuery -->
<script src="frontend/assets/js/jquery-3.6.0.min.js"></script>
<!-- Font Awesome -->
<script defer src="frontend/assets/js/all.min.js"></script>
<!-- Custom scripts -->
<script src="frontend/assets/js/script.js"></script>

</body>

</html>
