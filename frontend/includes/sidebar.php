<?php ?>
<aside class="sidebar">
    <div class="sidebar-start">
        <div class="sidebar-head">
            <a href="/" class="logo-wrapper" title="Home">
                <span class="sr-only"> صفحه اصلی </span>
                <span class="icon logo" aria-hidden="true"></span>
                <div class="logo-text">
                    <span class="logo-title">دکتر HI</span>
                    <span class="logo-subtitle">پنل مدیریت</span>
                </div>

            </a>
            <button class="sidebar-toggle transparent-btn" title="Menu" type="button">
                <span class="sr-only"> منو تاگل </span>
                <span class="icon menu-toggle" aria-hidden="true"></span>
            </button>
        </div>
        <div class="sidebar-body">
            <ul class="sidebar-body-menu">
                <li>
                    <a class="active" href="../app/"><span class="icon home" aria-hidden="true"></span>داشبورد</a>
                </li>
                <li>
                    <a class="show-cat-btn" href="#">
                        <span class="icon document" aria-hidden="true"></span> مدیریت درمانگاه ها
                        <span class="category__btn transparent-btn" title="Open list">
                        <span class="sr-only"> لیست باز </span>
                        <span class="icon arrow-down" aria-hidden="true"></span>
                        </span>
                    </a>
                    <ul class="cat-sub-menu">
                        <li>
                            <a href="clinics.php">لیست درمانگاه ها</a>
                        </li>
                        <li>
                            <a href="clinicdeleted.php">درمانگاه های حذف شده</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a class="show-cat-btn" href="#">
                        <span class="icon image" aria-hidden="true"></span> مدیریت بخش ها
                        <span class="category__btn transparent-btn" title="Open list">
                            <span class="sr-only"> لیست باز </span>
                            <span class="icon arrow-down" aria-hidden="true"></span>
                        </span>
                    </a>
                    <ul class="cat-sub-menu">
                        <li>
                            <a href="media-01.html">لیست بخش ها</a>
                        </li>
                        <li>
                            <a href="media-02.html">اضافه کردن بخش جدید</a>
                        </li>
                    </ul>
                </li>
                <!-- <li>
                    <a class="show-cat-btn" href="#">
                        <span class="icon paper" aria-hidden="true"></span> صفحات
                        <span class="category__btn transparent-btn" title="Open list">
                            <span class="sr-only"> لیست باز </span>
                            <span class="icon arrow-down" aria-hidden="true"></span>
                        </span>
                    </a>
                    <ul class="cat-sub-menu">
                        <li>
                            <a href="pages.html"> همه صفحات </a>
                        </li>
                        <li>
                            <a href="new-page.html"> صفحه جدید </a>
                        </li>
                    </ul>
                </li> -->

                <li>
                    <a class="show-cat-btn" href="#">
                        <span class="icon folder" aria-hidden="true"></span> اطلاعیه ها
                        <span class="category__btn transparent-btn" title="Open list">
                        <span class="sr-only"> لیست باز </span>
                        <span class="icon arrow-down" aria-hidden="true"></span>
                        </span>
                    </a>
                    <ul class="cat-sub-menu">
                        <li>
                            <a href="categories.html"> همه اطلاعیه ها </a>
                        </li>
                        <li>
                            <a href="categories.html"> اضافه کردن اطلاعیه جدید </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="comments.html">
                        <span class="icon message" aria-hidden="true"></span>
                        صندوق ورودی
                    </a>
                    <span class="msg-counter"> 7</span>
                </li>
            </ul>
            <span class="system-menu__title"> سیستم</span>
            <ul class="sidebar-body-menu">
                <!--
                <li>
                    <a href="appearance.html"><span class="icon edit" aria-hidden="true"></span> ظاهر</a>
                </li>
                <li>
                    <a class="show-cat-btn" href="##">
                        <span class="icon category" aria-hidden="true"></span> افزونه ها
                        <span class="category__btn transparent-btn" title = "Open list" >
                        <span class="sr-only"> Open list</span>
                        <span class="icon arrow-down" aria-hidden="true"></span>
                        </span>
                    </a>
                    <ul class="cat-sub-menu">
                        <li>
                            <a href="extention-01.html"> افزونه - 01</a>
                        </li>
                        <li>
                            <a href="extention-02.html"> افزونه - 02</a>
                        </li>
                    </ul>
                </li> -->
                <li>
                    <a class="show-cat-btn" href="#">
                        <span class="icon user-3" aria-hidden="true"></span> مدیریت بیماران
                        <span class="category__btn transparent-btn" title="Open list">
                            <span class="sr-only"> Open list</span>
                            <span class="icon arrow-down" aria-hidden="true"></span>
                        </span>
                    </a>
                    <ul class="cat-sub-menu">
                        <li>
                            <a href="users-01.html">لیست بیماران</a>
                        </li>
                        <li>
                            <a href="users-02.html">اضافه کردن بیمار جدید</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="show-cat-btn" href="#">
                        <span class="icon user-3" aria-hidden="true"></span> مدیریت کارکنان
                        <span class="category__btn transparent-btn" title="Open list">
                            <span class="sr-only"> Open list</span>
                            <span class="icon arrow-down" aria-hidden="true"></span>
                        </span>
                    </a>
                    <ul class="cat-sub-menu">
                        <li>
                            <a href="users-01.html">لیست کارکنان</a>
                        </li>
                        <li>
                            <a href="users-02.html">اضافه کردن کادر جدید</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a class="show-cat-btn" href="#">
                        <span class="icon user-3" aria-hidden="true"></span> مدیریت ادمین ها
                        <span class="category__btn transparent-btn" title="Open list">
                            <span class="sr-only"> Open list</span>
                            <span class="icon arrow-down" aria-hidden="true"></span>
                        </span>
                    </a>
                    <ul class="cat-sub-menu">
                        <li>
                            <a href="admins.php">لیست ادمین ها</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><span class="icon setting" aria-hidden="true"></span> تنظیمات</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="sidebar-footer">
        <a href="#" class="sidebar-user">
            <span class="sidebar-user-img">
                <picture>
                    <source srcset="./img/avatar/avatar-illustrated-01.webp" type="image/webp">
                    <img src="./img/avatar/avatar-illustrated-01.png" alt="User name">
                </picture>
            </span>
            <div class="sidebar-user-info">
                <span class="sidebar-user__title"> یونس سهرابی </span>
                <span class="sidebar-user__subtitle"> مدیر</span>
            </div>
        </a>
    </div>
</aside>
