<div class="main-wrapper">
    <!-- ! Main nav -->
    <nav class="main-nav--bg">
        <div class="container main-nav">
            <div class="main-nav-start">
                <div class="search-wrapper">
                    <i data-feather="search" aria-hidden="true"></i>
                    <input type="text" placeholder="جستجو کنید ..." required>
                </div>
            </div>
            <div class="main-nav-end">
                <button class="sidebar-toggle transparent-btn" title="Menu" type="button">
                    <span class="icon menu-toggle--gray" aria-hidden="true"></span>
                </button>

                <button class="theme-switcher gray-circle-btn" type="button" title="Switch theme">
                    <i class="sun-icon" data-feather="sun" aria-hidden="true"></i>
                    <i class="moon-icon" data-feather="moon" aria-hidden="true"></i>
                </button>
                <div class="notification-wrapper">
                    <button class="gray-circle-btn dropdown-btn" title="To messages" type="button">
                        <span class="icon notification active" aria-hidden="true"></span>
                    </button>
                    <ul class="users-item-dropdown notification-dropdown dropdown">
                        <li>
                            <a href="#">
                                <div class="notification-dropdown-icon info">
                                    <i data-feather="check"></i>
                                </div>
                                <div class="notification-dropdown-text">
                                    <span class="notification-dropdown__title">سیستم باید آپدیت شود</span>
                                    <span class="notification-dropdown__subtitle">سیستم آپدیت شد</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="notification-dropdown-icon danger">
                                    <i data-feather="info" aria-hidden="true"></i>
                                </div>
                                <div class="notification-dropdown-text">
                                    <span class="notification-dropdown__title">کش پر است!</span>
                                    <span class="notification-dropdown__subtitle">ما به فضای بیشتری نیاز داریم</span>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a class="link-to-page" href="#">بازکردن صفحه اعلانات</a>
                        </li>
                    </ul>
                </div>
                <div class="nav-user-wrapper">
                    <button href="#" class="nav-user-btn dropdown-btn" title="My profile" type="button">
                        <span class="nav-user-img">
                            <picture>
                                <source srcset="./img/avatar/avatar-illustrated-02.webp" type="image/webp">
                                <img src="./img/avatar/avatar-illustrated-02.png" alt="User name">
                            </picture>
                        </span>
                    </button>
                    <ul class="users-item-dropdown nav-user-dropdown dropdown">
                        <li><a href="#">
                                <i data-feather="user" aria-hidden="true"></i>
                                <span>پروفایل</span>
                            </a></li>
                        <li><a href="#">
                                <i data-feather="settings" aria-hidden="true"></i>
                                <span>تنظیمات اکانت</span>
                            </a></li>
                        <li><a class="danger" href="#">
                                <i data-feather="log-out" aria-hidden="true"></i>
                                <span>خروج</span>
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- ! Main -->
    <main class="main users chart-page" id="skip-target">
        <div class="container">
            <h2 class="main-title">لیست درمانگاه ها</h2>

            <form class="form" action="validController.php" method="POST">
                <label class="form-label-wrapper col-6">
                    <p class="form-label">نام :</p>
                    <input class="form-input" type="text" name="clinic_name" placeholder="نام درمانگاه را وارد کنید ..."
                           required>
                </label>
                <label class="form-label-wrapper col-6">
                    <p class="form-label">شماره تلفن</p>
                    <input class="form-input" type="text" name="clinic_phone"
                           placeholder="شماره تلفن درمانگاه را وارد نمایید ..." required>
                </label>
                <label class="form-label-wrapper col-12">
                    <p class="form-label">آدرس :</p>
                    <input class="form-input" type="text" name="clinic_address"
                           placeholder="آدرس درمانگاه را وارد نمایید ..." required>
                </label>


                <label class="form-checkbox-wrapper">
                    <input class="form-checkbox" type="checkbox" name="clinic_fTime">
                    <span class="form-checkbox-label">شبانه روزی</span>
                </label>

                <button class="form-btn primary-default-btn transparent-btn" name="addClinicBTN">ثبت درمانگاه</button>
            </form>

            <div class="row">
                <div class="col-lg-12">
                    <div class="users-table table-wrapper">
                        <table class="posts-table">
                            <thead>
                            <tr class="users-table-info">
                                <th>
                                    <label class="users-table__checkbox ms-20">
                                        <input type="checkbox" class="check-all">نام درمانگاه
                                    </label>
                                </th>
                                <th>آدرس درمانگاه</th>
                                <th>وضعیت</th>
                                <th>شماره تلفن</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $clinicList = (new Clinic())->getAllClinic();
                            foreach ($clinicList as $clinic){
                                if ($clinic->deleted_at != null){
                                    continue;
                                }

                            ?>
                            <tr id="idClinic<?php echo $clinic->id ?>">
                                <td>
                                    <label class="users-table__checkbox">
                                        <input type="checkbox" class="check">
                                        <span id="clinic__name"><?php echo $clinic->name ?></span>
                                    </label>
                                </td>
                                <td>
                                    <span id="clinic__address"><?php echo $clinic->address ?></span>
                                </td>
                                <td>

                                        <?php
                                        $status = $clinic->is_active;
                                        if ($status){ ?>
                                            <span class="badge-active">فعال</span>
                                        <?php } ?>

                                </td>
                                <td><span id="clinic__phone"><?php echo $clinic->phone ?></span></td>
                                <td>
                                        <span class="p-relative">
                                            <button class="dropdown-btn transparent-btn" type="button"
                                                    title="More info">
                                                <div class="sr-only">اطلاعات بیشتر</div>
                                                <i data-feather="more-horizontal" aria-hidden="true"></i>
                                            </button>
                                            <ul class="users-item-dropdown dropdown">
                                                <li><a href="#">ویرایش</a></li>
                                                <li><a style="cursor: pointer" onclick="deleteClinic(this.id);" id="<?php echo $clinic->id ?>">حذف</a></li>
<!--                                                <li><a href="validController.php?clinicDelID=--><?php //echo $clinic->id ?><!--">حذف</a></li>-->
                                            </ul>
                                        </span>
                                </td>
                            </tr><?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
