<?php

?>
<div class="main-wrapper">
    <!-- ! Main nav -->
    <nav class="main-nav--bg">
        <div class="container main-nav">
            <div class="main-nav-start">
                <div class="search-wrapper">
                    <i data-feather="search" aria-hidden="true"></i>
                    <input type="text" placeholder="جستجو کنید ..." required>
                </div>
            </div>
            <div class="main-nav-end">
                <button class="sidebar-toggle transparent-btn" title="Menu" type="button">
                    <span class="sr-only">تاگل منو</span>
                    <span class="icon menu-toggle--gray" aria-hidden="true"></span>
                </button>

                <button class="theme-switcher gray-circle-btn" type="button" title="Switch theme">
                    <span class="sr-only">Switch theme</span>
                    <i class="sun-icon" data-feather="sun" aria-hidden="true"></i>
                    <i class="moon-icon" data-feather="moon" aria-hidden="true"></i>
                </button>
                <div class="notification-wrapper">
                    <button class="gray-circle-btn dropdown-btn" title="To messages" type="button">
                        <span class="sr-only">To messages</span>
                        <span class="icon notification active" aria-hidden="true"></span>
                    </button>
                    <ul class="users-item-dropdown notification-dropdown dropdown">
                        <li>
                            <a href="#">
                                <div class="notification-dropdown-icon info">
                                    <i data-feather="check"></i>
                                </div>
                                <div class="notification-dropdown-text">
                                    <span class="notification-dropdown__title">سیستم باید آپدیت شود</span>
                                    <span class="notification-dropdown__subtitle">سیستم آپدیت شد</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="notification-dropdown-icon danger">
                                    <i data-feather="info" aria-hidden="true"></i>
                                </div>
                                <div class="notification-dropdown-text">
                                    <span class="notification-dropdown__title">کش پر است!</span>
                                    <span class="notification-dropdown__subtitle">ما به فضای بیشتری نیاز داریم</span>
                                </div>
                            </a>
                        </li>

                        <li>
                            <a class="link-to-page" href="#">بازکردن صفحه اعلانات</a>
                        </li>
                    </ul>
                </div>
                <div class="nav-user-wrapper">
                    <button href="#" class="nav-user-btn dropdown-btn" title="My profile" type="button">
                        <span class="sr-only">My profile</span>
                        <span class="nav-user-img">
            <picture>
                <source srcset="./img/avatar/avatar-illustrated-02.webp" type="image/webp">
                <img src="./img/avatar/avatar-illustrated-02.png" alt="User name">
            </picture>
          </span>
                    </button>
                    <ul class="users-item-dropdown nav-user-dropdown dropdown">
                        <li><a href="#">
                                <i data-feather="user" aria-hidden="true"></i>
                                <span>پروفایل</span>
                            </a></li>
                        <li><a href="#">
                                <i data-feather="settings" aria-hidden="true"></i>
                                <span>تنظیمات اکانت</span>
                            </a></li>
                        <li><a class="danger" href="#">
                                <i data-feather="log-out" aria-hidden="true"></i>
                                <span>خروج</span>
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- ! Main -->
    <main class="main users chart-page" id="skip-target">
        <div class="container">
            <h2 class="main-title">داشبورد</h2>
            <div class="row stat-cards">
                <div class="col-md-6 col-xl-3">
                    <article class="stat-cards-item">
                        <div class="stat-cards-icon primary">
                            <i data-feather="bar-chart-2" aria-hidden="true"></i>
                        </div>
                        <div class="stat-cards-info">
                            <p class="stat-cards-info__num">1478 286</p>
                            <p class="stat-cards-info__title">جمع بازدید</p>
                            <p class="stat-cards-info__progress">
                  <span class="stat-cards-info__profit success">
                    <i data-feather="trending-up" aria-hidden="true"></i>4.07%
                  </span>
                                ماه آخر
                            </p>
                        </div>
                    </article>
                </div>
                <div class="col-md-6 col-xl-3">
                    <article class="stat-cards-item">
                        <div class="stat-cards-icon warning">
                            <i data-feather="file" aria-hidden="true"></i>
                        </div>
                        <div class="stat-cards-info">
                            <p class="stat-cards-info__num">1478 286</p>
                            <p class="stat-cards-info__title">تست</p>
                            <p class="stat-cards-info__progress">
                  <span class="stat-cards-info__profit success">
                    <i data-feather="trending-up" aria-hidden="true"></i>0.24%
                  </span>
                                تست
                            </p>
                        </div>
                    </article>
                </div>
                <div class="col-md-6 col-xl-3">
                    <article class="stat-cards-item">
                        <div class="stat-cards-icon purple">
                            <i data-feather="file" aria-hidden="true"></i>
                        </div>
                        <div class="stat-cards-info">
                            <p class="stat-cards-info__num">1478 286</p>
                            <p class="stat-cards-info__title">تست</p>
                            <p class="stat-cards-info__progress">
                  <span class="stat-cards-info__profit danger">
                    <i data-feather="trending-down" aria-hidden="true"></i>1.64%
                  </span>
                                تست
                            </p>
                        </div>
                    </article>
                </div>
                <div class="col-md-6 col-xl-3">
                    <article class="stat-cards-item">
                        <div class="stat-cards-icon success">
                            <i data-feather="feather" aria-hidden="true"></i>
                        </div>
                        <div class="stat-cards-info">
                            <p class="stat-cards-info__num">1478 286</p>
                            <p class="stat-cards-info__title">تست</p>
                            <p class="stat-cards-info__progress">
                  <span class="stat-cards-info__profit warning">
                    <i data-feather="trending-up" aria-hidden="true"></i>0.00%
                  </span>
                                تست
                            </p>
                        </div>
                    </article>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-9">
                    <div class="chart">
                        <canvas id="myChart" aria-label="Site statistics" role="img"></canvas>
                    </div>
                    <div class="users-table table-wrapper">
                        <table class="posts-table">
                            <thead>
                            <tr class="users-table-info">
                                <th>
                                    <label class="users-table__checkbox ms-20">
                                        <input type="checkbox" class="check-all">انتخاب همه
                                    </label>
                                </th>
                                <th>عنوان</th>
                                <th>نویسنده</th>
                                <th>وضعیت</th>
                                <th>تاریخ</th>
                                <th>عملیات</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <label class="users-table__checkbox">
                                        <input type="checkbox" class="check">
                                        <div class="categories-table-img">
                                            <picture>
                                                <source srcset="./img/categories/01.webp" type="image/webp">
                                                <img src="./img/categories/01.jpg" alt="category"></picture>
                                        </div>
                                    </label>
                                </td>
                                <td>
                                    این یک متن تست 1 است
                                </td>
                                <td>
                                    <div class="pages-table-img">
                                        <picture>
                                            <source srcset="./img/avatar/avatar-face-04.webp" type="image/webp">
                                            <img src="./img/avatar/avatar-face-04.png" alt="User Name"></picture>
                                        اسم تست 1
                                    </div>
                                </td>
                                <td><span class="badge-pending">درانتظار</span></td>
                                <td>17.04.2021</td>
                                <td>
                      <span class="p-relative">
                        <button class="dropdown-btn transparent-btn" type="button" title="More info">
                          <div class="sr-only">اطلاعات بیشتر</div>
                          <i data-feather="more-horizontal" aria-hidden="true"></i>
                        </button>
                        <ul class="users-item-dropdown dropdown">
                          <li><a href="#">ویرایش</a></li>
                          <li><a href="#">ویرایش سریع</a></li>
                          <li><a href="#">حذف</a></li>
                        </ul>
                      </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label class="users-table__checkbox">
                                        <input type="checkbox" class="check">
                                        <div class="categories-table-img">
                                            <picture>
                                                <source srcset="./img/categories/02.webp" type="image/webp">
                                                <img src="./img/categories/02.jpg" alt="category"></picture>
                                        </div>
                                    </label>
                                </td>
                                <td>
                                    متن تست 2 برای داخل جدول
                                </td>
                                <td>
                                    <div class="pages-table-img">
                                        <picture>
                                            <source srcset="./img/avatar/avatar-face-03.webp" type="image/webp">
                                            <img src="./img/avatar/avatar-face-03.png" alt="User Name"></picture>
                                        تست 2
                                    </div>
                                </td>
                                <td><span class="badge-pending">درانتظار</span></td>
                                <td>23.04.2021</td>
                                <td>
                      <span class="p-relative">
                        <button class="dropdown-btn transparent-btn" type="button" title="More info">
                          <div class="sr-only">More info</div>
                          <i data-feather="more-horizontal" aria-hidden="true"></i>
                        </button>
                        <ul class="users-item-dropdown dropdown">
                          <li><a href="#">ویرایش</a></li>
                          <li><a href="#">ویرایش سریع</a></li>
                          <li><a href="#">حذف</a></li>
                        </ul>
                      </span>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <label class="users-table__checkbox">
                                        <input type="checkbox" class="check">
                                        <div class="categories-table-img">
                                            <picture>
                                                <source srcset="./img/categories/04.webp" type="image/webp">
                                                <img src="./img/categories/04.jpg" alt="category"></picture>
                                        </div>
                                    </label>
                                </td>
                                <td>
                                    متن تست 3
                                </td>
                                <td>
                                    <div class="pages-table-img">
                                        <picture>
                                            <source srcset="./img/avatar/avatar-face-05.webp" type="image/webp">
                                            <img src="./img/avatar/avatar-face-05.png" alt="User Name"></picture>
                                        تست3
                                    </div>
                                </td>
                                <td><span class="badge-active">فعال</span></td>
                                <td>17.04.2021</td>
                                <td>
                      <span class="p-relative">
                        <button class="dropdown-btn transparent-btn" type="button" title="More info">
                          <div class="sr-only">More info</div>
                          <i data-feather="more-horizontal" aria-hidden="true"></i>
                        </button>
                        <ul class="users-item-dropdown dropdown">
                          <li><a href="#">ادیت</a></li>
                          <li><a href="#">ادیت سریع</a></li>
                          <li><a href="#">حذف</a></li>
                        </ul>
                      </span>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-3">
                    <article class="customers-wrapper">
                        <canvas id="customersChart" aria-label="Customers statistics" role="img"></canvas>
                        <!--              <p class="customers__title">New Customers <span>+958</span></p>
                        <p class="customers__date">28 Daily Avg.</p>
                        <picture><source srcset="./img/svg/customers.svg" type="image/webp"><img src="./img/svg/customers.svg" alt=""></picture> -->
                    </article>
                    <article class="white-block">
                        <div class="top-cat-title">
                            <h3>بهترین دسته ها</h3>
                            <p>28 دسته, 1400 پست</p>
                        </div>
                        <ul class="top-cat-list">
                            <li>
                                <a href="#">
                                    <div class="top-cat-list__title">
                                        لایف استایل <span>8.2k</span>
                                    </div>
                                    <div class="top-cat-list__subtitle">
                                        مقالات روزانه سبک زندگی <span class="purple">+472</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="top-cat-list__title">
                                        آموزش ها <span>8.2k</span>
                                    </div>
                                    <div class="top-cat-list__subtitle">
                                        دوره های برنامه نویسی <span class="blue">+472</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="top-cat-list__title">
                                        تکنولوژی <span>8.2k</span>
                                    </div>
                                    <div class="top-cat-list__subtitle">
                                        مقالات روزانه تکنولوژی <span class="danger">+472</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="top-cat-list__title">
                                        UX طراحی <span>8.2k</span>
                                    </div>
                                    <div class="top-cat-list__subtitle">
                                        UX نکات طراحی <span class="success">+472</span>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="top-cat-list__title">
                                        نکات اینترکشن <span>8.2k</span>
                                    </div>
                                    <div class="top-cat-list__subtitle">
                                        مقالات اینترکشن <span class="warning">+472</span>
                                    </div>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <div class="top-cat-list__title">
                                        گرافیک <span>8.2k</span>
                                    </div>
                                    <div class="top-cat-list__subtitle">
                                        مقالات گرافیک <span class="primary">+472</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </article>
                </div>
            </div>
        </div>
    </main>
</div>
