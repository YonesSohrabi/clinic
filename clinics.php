<?php
    // Header
    require_once "frontend/includes/header.php";
    // Sidebar
    require_once "frontend/includes/sidebar.php";
    // main-wrapper
    require_once "frontend/pages/clinics.php";
    // Footer
    require_once "frontend/includes/footer.php";
